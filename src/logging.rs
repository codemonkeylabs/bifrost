pub use log::LevelFilter;
use crate::Error;
use flexi_logger::{self, LogSpecBuilder, LogSpecification, ModuleFilter, colored_detailed_format};
use libc::c_char;
use log;
use std::fmt::Display;

#[no_mangle]
pub extern "C" fn c_logger(level:u8, c_component:*const c_char, c_buf:*const c_char) {
    unsafe {
        let component = std::ffi::CStr::from_ptr(c_component).to_str().unwrap_or("<unknown>");
        let buf       = std::ffi::CStr::from_ptr(c_buf).to_str().unwrap_or("<invalid data>");
        match level {
            0 => log::trace!(target: component, "{}", buf),
            1 => log::debug!(target: component, "{}", buf),
            2 => log::info!(target: component, "{}", buf),
            3 => log::warn!(target: component, "{}", buf),
            _ => log::error!(target: component, "{}", buf)
        }
    }
}

pub struct Logger {
    pub logger  : flexi_logger::LoggerHandle
  , pub builder : LogSpecBuilder
}

impl Logger {
    pub fn set_target<T : AsRef<str>>(&mut self, module : Option<T>, level : LevelFilter) {
        match module {
            None         => self.builder.default(level),
            Some(target) => self.builder.module(target, level)
        };
        self.logger.set_new_spec(self.builder.build())
    }
    pub fn remove_target<T : AsRef<str>>(&mut self, module : T) {
        self.builder.remove(module);
        self.logger.set_new_spec(self.builder.build())
    }
}

pub fn parse_spec<T: AsRef<str> + Display + Copy>(s: T) -> Result<Vec<(Option<String>, LevelFilter)>, Error> {
    let spec = LogSpecification::parse(s)
                .map_err(|err| Error::InvalidData(format!("Invalid logging spec {}: {}", s, err)))?;
    Ok(spec.module_filters()
        .into_iter()
        .map(|ModuleFilter{module_name, level_filter}| ((*module_name).clone(), (*level_filter).clone()))
        .collect())
}

pub fn parse_modules(s: &str) -> Vec<String> {
    s.split(',').map(|p| p.trim().to_string()).collect()
}

pub fn init(spec_arg : Option<&str>) -> Logger {
    let spec = match spec_arg.map(LogSpecification::parse) {
                 None    => LogSpecification::env()
                              .or_else(|_| LogSpecification::parse("warn"))
                              .expect("Unable to figure out logging configuration"),
                 Some(x) => x.expect("Invalid logging configuration specified")
               };
    let mut builder = LogSpecification::builder();
    builder.insert_modules_from(spec);
    let logger = flexi_logger::Logger::with(builder.build())
                   .format_for_stderr(colored_detailed_format)
                   .start()
                   .expect("Failed to initialize logging");
    Logger{logger, builder}
}

