use crate::Error;
use crate::interface;
use crate::interface::queue::Queue;
use crate::packet::Packet;
use crate::shared_ptr::{self, Shared};
use libc::c_void;
use std::collections::HashMap;
use std::thread;
use tokio::{runtime, task};
use tokio::sync::mpsc;

const MAX_OUTSTANDING_CONTROL_MESSAGES : usize = 16;

struct IntfHandle {
    intf : interface::Ptr
  , task : task::JoinHandle<Result<(), Error>>
}

pub enum Message {
    Connect(interface::Ptr),
    Disconnect(interface::Id),
    Terminate
}

enum Answer {
    Okay
}

trait Reply {
    fn reply(self) -> Result<Answer, Error>;
}

impl Reply for Result<(), Error> {
    fn reply(self) -> Result<Answer, Error> {
        self.map(|()| Answer::Okay)
    }
}

struct Courier {
    msg:    Message,
    ack_tx: mpsc::Sender<Result<Answer, Error>>
}

pub struct Switch {
    interfaces : HashMap<interface::Id, IntfHandle>
}

pub struct Handle {
    tx_ch : mpsc::Sender<Courier>
}

#[repr(C)]
pub struct RxContext<'a> {
    pub switch:     &'a Shared<Switch>
  , pub input_intf: &'a interface::Ptr
}

pub fn connect(switch: &Shared<Switch>, intf: &interface::Ptr) {
    let locked_intf = intf.read().unwrap();
    let key         = locked_intf.id().clone();
    let task = task::spawn(locked_intf.service(switch.clone(), Switch::c_rx_packet));
    switch.write().unwrap().interfaces.insert(key, IntfHandle{task, intf: intf.clone()});
}

fn disconnect(switch: &Shared<Switch>, id : &interface::Id) {
    let mut locked = switch.write().unwrap();
    locked.interfaces.get(id).map(|intf| intf.task.abort());
    locked.interfaces.remove(id);
}

impl Switch {
    #[no_mangle]
    pub unsafe extern "C" fn c_rx_packet(ctx: *mut c_void, pkt: Packet) {
        let RxContext{switch, input_intf} = &*(ctx as *mut RxContext);
        switch.read().unwrap().rx_packet(input_intf, pkt);
    }
    pub fn rx_packet(&self, input_intf: &interface::Ptr, pkt: Packet) {
        log::trace!(target: "switch", "{} -> {}", input_intf.read().unwrap().id(), pkt);
    }

    pub fn spawn() -> Handle {
        let (tx_ch, mut rx_ch) = mpsc::channel(MAX_OUTSTANDING_CONTROL_MESSAGES);
        thread::spawn(|| {
          let status : Result<(), Error> = runtime::Builder::new_multi_thread()
            .enable_all()
            .thread_name("bifrost switch")
            .build()
            .unwrap()
            .block_on(async move {
                let switch = shared_ptr::new(Switch{ interfaces: HashMap::new() });
                loop {
                    let Courier{ack_tx, msg} = rx_ch.recv()
                                                .await
                                                .ok_or(Error::CommsFailure("Switch channel has been closed".to_string()))?;
                    match msg {
                        Message::Connect(interface) => {
                            connect(&switch, &interface);
                            ack_tx.send(Ok(Answer::Okay)).await?;
                        }
                        Message::Disconnect(id) => {
                            disconnect(&switch, &id);
                            ack_tx.send(Ok(Answer::Okay)).await?;

                        }
                        Message::Terminate => {
                            for (_, IntfHandle{task, ..}) in &switch.write().unwrap().interfaces {
                                task.abort()
                            }
                            ack_tx.send(Ok(Answer::Okay)).await?;
                            return Err(Error::Aborted("Switch has been terminated".to_string()))
                        }
                    }
                }
            });
          log::error!(target: "switch", "Switch shutdown: {:?}", status)
        });
        return Handle{tx_ch}
    }

    pub fn get_intf(&self, id: &interface::Id) -> Option<interface::Ptr> {
        self.interfaces.get(id).map(|handle| handle.intf.clone())
    }
}

impl Handle {
    async fn send_message(&mut self, msg : Message) -> Result<(), Error> {
        let (ack_tx, mut ack_rx) = mpsc::channel(1);
        self.tx_ch.send(Courier{msg, ack_tx}).await?;
        ack_rx.recv()
            .await
            .ok_or(Error::CommsFailure("Switch has closed the channel".to_string()))?
            .and_then(|_| Ok(()))
    }
    pub async fn attach_interface(&mut self, id: &interface::Id, device: Option<String>, filter_exp: Option<String>) -> Result<(), Error> {
        let interface = create_interface(id, device, filter_exp)?;
        self.send_message(Message::Connect(interface)).await
    }
}

fn create_interface(id: &interface::Id, device: Option<String>, filter_exp: Option<String>) -> Result<interface::Ptr, Error> {
    match id.kind() {
        interface::Kind::Queue => Ok(shared_ptr::new(Queue::init(id, device, filter_exp)?)),
        interface::Kind::VIF   => Err(Error::Unimplemented("VIF interface support is missing".to_string())),
        interface::Kind::GRE   => Err(Error::Unimplemented("GRE tunnels not supported".to_string()))
    }
}
