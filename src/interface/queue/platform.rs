#[cfg(not(target_os = "linux"))]
mod pcap;
#[cfg(not(target_os = "linux"))]
pub use pcap::*;
#[cfg(target_os = "linux")]
mod nfq;
#[cfg(target_os = "linux")]
pub use nfq::*;