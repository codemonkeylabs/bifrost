use async_zmq::{self, Multipart};
use crate::Error;
use crate::mqs::{Response};
use futures::future::{self, FutureExt, TryFutureExt};
use log;

pub async fn run(endpoint : &str, mut handle : impl FnMut(Multipart) -> Response<()>) -> Result<(), Error>
{
    log::debug!(target: "butler", "running ØMQ REPLY socket @{}", endpoint);
    let rep = async_zmq::reply(endpoint)?.bind()?;

    loop {
        let msg = rep.recv().map_err(Error::from).await?;
        let Response{reply,result} = handle(msg);
        log::debug!(target: "debug", "handled msg: {:?}", result);
        rep.send(reply.unwrap_or(vec![])).then(|status| {
            match status {
                Ok(_) => match result {
                            Ok(v)    => future::ok(v),
                            Err(err) => future::err(err)
                         },
                Err(err) => future::err(Error::from(err))
            }
        }).await?
    }
}

