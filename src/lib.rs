pub mod controller;
pub mod interface;
pub mod logging;
pub mod mqs;
pub mod packet;
pub mod switch;

use async_zmq as zmq;
use ciborium as cbor;
use tokio::io::unix::TryIoError;
use tokio::sync::mpsc;

#[derive(Clone)]
pub enum Error {
	  Aborted(String)
	, CommsFailure(String)
	, InvalidArgument(String)
	, InternalFailure(String)
	, InvalidData(String)
	, IOError(String)
	, NoSuchDevice(String)
	, Timeout(String)
	, Unexpected(String)
	, Unimplemented(String)
}

pub struct Env {
	pub logger: logging::Logger
  , pub switch: switch::Handle
}

impl std::fmt::Display for Error {
	fn fmt(&self, f : &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			  Error::Aborted(msg)          => write!(f, "Aborted: {}", msg)
			, Error::CommsFailure(msg)     => write!(f, "Communication Failure: {}", msg)
			, Error::InternalFailure(msg)  => write!(f, "Internal Failure: {}", msg)
			, Error::InvalidArgument(msg)  => write!(f, "Invalid argument: {}", msg)
			, Error::InvalidData(arg)      => write!(f, "Invalid Data: {}", arg)
			, Error::IOError(err)          => write!(f, "I/O Error: {}", err)
			, Error::NoSuchDevice(dev)     => write!(f, "No Such Device: {}", dev)
			, Error::Timeout(msg)		   => write!(f, "Timeout: {}", msg)
			, Error::Unexpected(msg)       => write!(f, "Unexpected {}", msg)
			, Error::Unimplemented(msg)    => write!(f, "Missing implementation: {}", msg)
		}
	}
}

impl From<Error> for std::fmt::Error {
	fn from(_: Error) -> Self {
		std::fmt::Error
	}
}

impl std::fmt::Debug for Error {
	fn fmt(&self, f : &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self)
	}
}
macro_rules! from_error {
	($error_type:path, $error_constructor:ident, $msg:expr) => {
		impl From<$error_type> for Error {
			fn from(e : $error_type) -> Error {
				Error::$error_constructor(format!($msg, e))
			}
		}
	};
}

#[macro_export]
macro_rules! show {
	($typ: path, $decl: item) => {
		impl std::fmt::Debug for $typ {
			fn fmt(&self, __f : &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
				write!(__f, "{}", self)
			}
		}
		impl std::fmt::Display for $typ {
			$decl
		}
	}
}

impl<T: std::fmt::Debug> From<cbor::de::Error<T>> for Error {
	fn from(e : cbor::de::Error<T>) -> Error {
		Error::InvalidData(format!("invalid CBOR data: {}", e))
	}
}

impl<T: std::fmt::Debug> From<cbor::ser::Error<T>> for Error {
	fn from(e : cbor::ser::Error<T>) -> Error {
		Error::InvalidData(format!("CBOR serialization failed: {}", e))
	}
}

impl<T> From<mpsc::error::SendError<T>> for Error {
	fn from(e : mpsc::error::SendError<T>) -> Error {
		Error::CommsFailure(format!("unable to send MPSC message: {}", e))
	}
}

from_error!{std::ffi::NulError     , InvalidData,  "invalid c string {}"}
from_error!{zmq::Error             , CommsFailure, "ØMQ error: {}"}
from_error!{zmq::RequestReplyError , CommsFailure, "ØMQ req/reply error: {}"}
from_error!{zmq::SocketError       , CommsFailure, "ØMQ socket error: {}"}
from_error!{std::io::Error         , IOError,      "I/O Error: {}"}
from_error!{std::str::Utf8Error    , InvalidData,  "invalid UTF-8 string {}"}
from_error!{std::num::ParseIntError, InvalidData,  "invalid numeric value {}"}
from_error!{TryIoError 			   , IOError,      "Async I/O Error {:?}"}

#[macro_export]
macro_rules! opaque_c_struct {
	($struct_name:ident) => {
	    #[repr(C)]
		pub struct $struct_name {
    			_data: [u8; 0],
    			_marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>
		}
	};
}

#[macro_export]
macro_rules! c_string {
	($slice:expr) => {
		std::ffi::CString::new($slice)?
	};
}

#[macro_export]
macro_rules! from_c_string {
	($ptr:expr) => {
		std::ffi::CStr::from_ptr($ptr).to_str()?.to_string()
	}
}

pub struct CBox<T>
{
    val: T,
    deallocator: Box<dyn Fn(&T) -> ()>
}

impl<T> CBox<T>
 {
    pub fn new(val: T, deallocator: Box<dyn Fn(&T) -> ()>) -> CBox<T> {
        CBox{val, deallocator}
    }
}

impl<T> Drop for CBox<T>
{
    fn drop(&mut self) {
        (self.deallocator)(&self.val);
    }
}

impl<T> std::ops::Deref for CBox<T>

{
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.val
    }
}

pub mod shared_ptr {
	use std::sync::{Arc, RwLock};
	pub type Shared<T> = Arc<RwLock<T>>;
	pub fn new<T>(val: T) -> Shared<T> { Arc::new(RwLock::new(val)) }
}




