mod platform;

use crate::*;
use crate::packet::Packet;
use crate::shared_ptr::Shared;
use crate::switch::Switch;
use interface::{self, FutureService, Interface};
use libc::c_void;
use platform::Platform;

#[allow(dead_code)]
pub struct Queue {
	id :      interface::Id,
	platform: Box<Platform>
}

impl Queue {
	pub fn init(id: &interface::Id, device:Option<String>, filter_exp: Option<String>) -> Result<Queue, Error> {
		let platform = platform::init(id, device, filter_exp)?;
		Ok(Queue{id: (*id).clone(), platform})
	}
}

impl Interface for Queue {
	fn id(&self) -> &interface::Id { &self.id }
	fn service(&self, switch: Shared<Switch>, rx_packet: unsafe extern "C" fn(ctx: *mut c_void, Packet)) -> FutureService {
		let intf = switch.read().unwrap().get_intf(&self.id).unwrap();
		self.platform.service(intf, switch, rx_packet)
	}
}