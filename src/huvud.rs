use bifroest::Error;
use bifroest::controller::{Message, encode, decode};
use bifroest::logging::{parse_spec, parse_modules};
use clap;
use zmq; 

const RESPONSE_TIMEOUT: i32 = 5000; // mS

fn main() -> Result <(), Error> {
    let yaml = clap::load_yaml!("huvud.yaml");
    let args = clap::App::from_yaml(yaml).get_matches();

    process_commands(args)?;
    Ok(println!("Ok👌"))
}

macro_rules! subcommand {
    ($cmd:ident, $n: literal, $f:expr) => {
        $cmd.subcommand_matches($n).map($f).unwrap_or(Ok(()))?
    }
}
fn process_commands(args : clap::ArgMatches) -> Result<(), Error> {
    let ctx = zmq::Context::new();
    let socket = ctx.socket(zmq::REQ)?;
    socket.set_rcvtimeo(RESPONSE_TIMEOUT)?;
    socket.set_linger(0)?;
    socket.connect(args.value_of("controller").unwrap())?;

    if let Some(cmd) = args.subcommand_matches("remove") {
        subcommand!(cmd, "log"      , remove_log_target(&socket));
        subcommand!(cmd, "interface", remove_interface(&socket))
    } else if let Some(cmd) = args.subcommand_matches("set") {
        subcommand!(cmd, "log"      , set_log_target(&socket));
        subcommand!(cmd, "interface", set_interface(&socket))
    } else if let Some(_) = args.subcommand_matches("shutdown") {
        shutdown(&socket)?;
    } else {
        return Err(Error::InvalidArgument("Nothing to do? 🤷‍♂️".to_string()))
    }

    Ok(())
}

fn request(msg : Message, socket : &zmq::Socket) -> Result<Message, Error> {
    let frame = encode(msg)?;
    socket.send(frame, 0)?;
    let mut frame = zmq::Message::new();
    match socket.recv(&mut frame, 0) {
        Err(err@zmq::Error::EAGAIN) =>
            return Err(Error::Timeout(format!("Bifröst is not responding: {}", err))),
        result =>
            result?
    }
    decode(&frame)
}

fn check(msg : Message) -> Box<dyn Fn (Message) -> Result<(), Error>> {
    Box::new(move |answer|
        if msg == answer {
            Ok(())
        } else {
            Err(Error::Unexpected(format!("answer: {:?}", answer)))
        })
}

fn set_log_target(socket: &zmq::Socket) -> impl FnOnce (&clap::ArgMatches) -> Result<(), Error> + '_ {
    | params : &clap::ArgMatches | {
        let targets = parse_spec(params.value_of("target").unwrap())?;
        return request(Message::SetLogTargets{targets}, socket).and_then(check(Message::Ack))
    }
}

fn remove_log_target(socket: &zmq::Socket) -> impl FnOnce (&clap::ArgMatches) -> Result<(), Error> + '_ {
    | params : &clap::ArgMatches | {
        let modules = parse_modules(params.value_of("target").unwrap());
        request(Message::RemoveLogTargets{modules}, socket).and_then(check(Message::Ack))
    }
}

fn set_interface(socket: &zmq::Socket) -> impl FnOnce (&clap::ArgMatches) -> Result<(), Error> + '_ {
    | params : &clap::ArgMatches | {
        let id         = params.value_of("id").unwrap().parse()?;
        let filter_exp = params.value_of("filter").map(|s| s.to_string());
        let device     = params.value_of("device").map(|s| s.to_string());
        return request(Message::AttachInterface{id, device, filter_exp}, socket).and_then(check(Message::Ack))
    }
}

fn remove_interface(socket: &zmq::Socket) -> impl FnOnce (&clap::ArgMatches) -> Result<(), Error> + '_ {
    | params : &clap::ArgMatches | {
        let id         = params.value_of("id").unwrap().parse()?;
        request(Message::DettachInterface{id}, socket).and_then(check(Message::Ack))
    }
}

fn shutdown(socket: &zmq::Socket) -> Result<(), Error> {
    request(Message::Shutdown, socket).and_then(check(Message::Ack))
}
